# Favourite Christmastime Snack Mix

A family favourite introduced by my Aunt Betty in the 1970s

__Author__ Jordon Davidson, December 2020

__Prep Time__ 5 Minutes

__Cook Time__ 1 Hour

## Ingredients

### Flavouring

* 1 Tbsp. Worchestershire sauce
* 1/2 tsp. Celery salt
* 1/2 tsp. Garlic salt
* 1/2 tsp. Onion salt
* 1/2 tsp. Paprika
* 1/4 cup Butter

### Snack Bits

* 2 cups Cheerios
* 2 cups Shreddies
* 2 cups Pretzel Sticks
* 1 1/2 cups Peanuts

## Instructions

Preheat oven to 250°F

In a glass 1 cup measure melt the butter in the microwave.

Add the spices and worchestershire sauce to the melted butter and mix together.

In a shallow 13" X 9" baking dish add the Snack Bits and thouroughly mix them together.

Pour the spice-butter mixture over the dry ingredients and mix together to coat them thoroughly.

Place in the oven. Stir the mix every 15 minutes.

Remove after an hour and allow to cool.

Enjoy!


