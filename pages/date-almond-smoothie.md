# Date Almond Smoothie

Makes 4 Servings. 

In a Vitamix this smoothie is made on the Hot setting. To make in a regular blender make the smoothie then heat in the microwave or in a saucepan on the stove on medium-low heat.

## Ingredients

- 2 Cups unsweetend almond milk
- 40 Raw almonds
- 5-7 pitted dates
- 1 medium to large bananna.
- 4 tbsp. Peanut Butter (pure peanut butter, like Kraft Just Peanuts)
- 4 cups boiled water
- 2 shakes allspice (appox. 1/4 tsp.)
- 2 shakes cinnamon

## Instructions

1. Boil the water
2. In a 2 cup measure add the almonds and dates. 
3. Fill the measuring cup with boiling water, reserving the remainder. Allow to sit for 5 minutes.
4. To the blender add:
    - Almond Milk,
    - Spices
    - Bananna
    - Peanut butter
    - Contents of the measuring cup.
5. Fill the blender to the 7 cup mark with the remaining boiled water.
6. Blend. with vitamix use the hot drink setting. On other blenders, blend on medium until nuts are fully chopped and blended in (2-3 min.)

### Optional.
- Pour into a saucepan and heat to hot chocolate temperatue on med low heat, stirring often. 

__OR__

- Pour into a Mug and microwave on high for 30-45 seconds.

